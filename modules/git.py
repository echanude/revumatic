import os
import pygit2
import subprocess
import urllib.parse

from modules import packages, patch, utils


class GitError(Exception):
    pass

class GitFatalError(GitError):
    pass

class GitConfigError(GitError):
    def __init__(self, msg, suggested_command=None, git_command=None):
        super().__init__(msg)
        self.suggested_command = suggested_command
        self.git_command = git_command

class GitNoRemote(GitError):
    pass

class GitBadProject(GitError):
    pass


class Repo:
    def __init__(self, path):
        self.path = os.path.expanduser(path)
        try:
            self.repo = pygit2.Repository(self.path)
        except pygit2.GitError:
            raise GitFatalError('\'{}\' is not a git repository.'.format(path))

    @classmethod
    def clone(cls, remote, path, output_callback=None, check=True):
        cls._run_git(['clone', '--progress', remote, path], None, output_callback,
                     check=check)
        return cls(path)

    @classmethod
    def _run_git(cls, args, path, output_callback, check):
        if path:
            args = ['git', '-C', path] + args
        else:
            args = ['git'] + args
        result = utils.run_cb(args, output_callback, git=True)
        if check and result != 0:
            raise GitFatalError('Failed to run git. The command was: {}'
                                .format(' '.join(args)))
        return result == 0

    def fetch(self, remote_name, output_callback=None, commit=None, check=True):
        args = ['fetch', '--progress', remote_name]
        if commit is not None:
            args.append(commit)
        return self._run_git(args, self.path, output_callback, check=check)

    def full_sha(self, revision):
        return str(self.repo.revparse_single(revision).id)


class ProjectRepo(Repo):
    def get_all_projects(self, labname):
        result = []
        for remote in self.repo.remotes:
            if remote.url.startswith('http'):
                parsed = urllib.parse.urlsplit(remote.url)
                server = parsed.netloc
                project = parsed.path
                if project.startswith('/'):
                    project = project[1:]
            else:
                try:
                    server, project = remote.url.split(':')
                    login, server = server.split('@')
                except ValueError:
                    continue
            if project.endswith('.git'):
                project = project[:-4]
            if server != labname:
                continue
            result.append((project, remote))
        return result

    def _get_mr_refs(self, labname, project_filter):
        """Returns two lists: a list of dictionaries with merge request
        refspecs, and a list of remotes (as a tuple (project, remote)) that
        do not have a merge request refspec set up. The project_filter is
        a function that gets the project name and should return True/False
        to consider/skip this remote."""
        mr_refs = []
        no_mr_ref = []
        for project, remote in self.get_all_projects(labname):
            if not project_filter(project):
                continue
            found = False
            for ref in remote.fetch_refspecs:
                if ref.startswith('+'):
                    ref = ref[1:]
                src, dst = ref.split(':')
                if src.startswith('refs/merge-requests/') and dst.endswith('/*'):
                    mr_refs.append({ 'remote': remote,
                                     'project': project,
                                     'short_project': project.split('/')[-1],
                                     'pkg': packages.find(project),
                                     'ref': dst[:-2] })
                    found = True
            if not found:
                no_mr_ref.append((project, remote))
        return mr_refs, no_mr_ref

    def _store_mr_ref(self, mr_ref):
        for k, v in mr_ref.items():
            setattr(self, k, v)

    def find_remote(self, labname, projectname):
        pkg = packages.find(projectname)
        if not pkg:
            raise GitBadProject('Unsupported project: {}'.format(projectname))

        mr_refs, no_mr_ref = self._get_mr_refs(labname,
                                               lambda project: project == projectname)
        if len(mr_refs) == 1:
            self._store_mr_ref(mr_refs[0])
            return self.pkg
        if len(mr_refs) > 1 or len(no_mr_ref) > 1:
            raise GitNoRemote('Multiple remotes matching {} found in repository \'{}\'.'
                              .format(projectname, self.path))
        if len(no_mr_ref) == 0:
            raise GitNoRemote('No remote matching {} found in repository \'{}\'.'
                              .format(projectname, self.path))

        # There's no merge requests refspec but we've got the right project
        # and remote. Store them.
        self.project = projectname
        self.remote = no_mr_ref[0][1]

        cmd = ['git']
        if self.path != '.':
            cmd.extend(('-C', self.path))
        cmd.extend(('config', '--add', 'remote.{}.fetch'.format(self.remote.name),
                    '+refs/merge-requests/*/head:refs/remotes/{}/merge-requests/*'.format(self.remote.name)))
        raise GitConfigError('No refspec with merge requests found. Please run:\n' + ' '.join(cmd),
                             cmd, 'config')

    def find_projects(self, labname):
        mr_refs, no_mr_ref = self._get_mr_refs(labname,
                                               lambda project: packages.find(project))
        result = [m['project'] for m in mr_refs]
        result.extend(m[0] for m in no_mr_ref)
        if not result:
            raise GitNoRemote('No remote with a downstream project found in repository \'{}\'.'
                              .format(self.path))
        result.sort()
        return result

    def fetch(self, output_callback=None, commit=None, check=True):
        return super().fetch(self.remote.name, output_callback, commit, check)

    def get_mr_head(self, mr_id):
        return self.full_sha('{}/{}'.format(self.ref, mr_id))

    def get_commits(self, output_callback, base, head):
        try:
            head = self.repo.revparse_single(head).id
        except KeyError:
            # try to fetch the head in case it is a historic commit that was
            # not fetched by the standard 'git fetch'
            self.fetch(output_callback, commit=head, check=False)
            head = self.repo.revparse_single(head).id
        base = self.repo.revparse_single(base).id
        walker = self.repo.walk(head)
        walker.sort(pygit2.GIT_SORT_TOPOLOGICAL | pygit2.GIT_SORT_REVERSE)
        walker.hide(base)
        return walker

    def get_diff(self, commit, silent=False):
        if not isinstance(commit, str):
            commit = str(commit.id)
        args = ('git', '-C', self.path, 'show', '-p', commit)
        result = utils.run(args, git='diff', check=False, universal_newlines=True,
                           stdout=subprocess.PIPE, stderr=subprocess.DEVNULL,
                           errors='replace')
        if result.returncode == 0:
            return result.stdout
        if silent:
            return ''
        raise KeyError('commit {} not present in {}'.format(commit, self.path))

    def get_diff_range(self, base, head):
        args = ('git', '-C', self.path, 'diff', '{}..{}'.format(base, head))
        for i in range(2):
            result = utils.run(args, git='diff', check=False, universal_newlines=True,
                               stdout=subprocess.PIPE, stderr=subprocess.DEVNULL,
                               errors='replace')
            if result.returncode == 0:
                return result.stdout
            # try to fetch the head in case it is a historic commit that was
            # not fetched by the standard 'git fetch'
            if i > 0 or not self.fetch(commit=head, check=False):
                result.check_returncode()
                assert False

    def get_parsed_diff(self, sha):
        return patch.PatchParser(sha[:12], self.get_diff(sha)).parse()

    def get_parsed_diff_range(self, base, head):
        return patch.PatchParser('{:.12}..{:.12}'.format(base, head),
                                 self.get_diff_range(base, head)).parse()


class RepoCollection:
    def __init__(self, paths, labname):
        self.repos = [self._new_repo(path) for path in paths]
        self.labname = labname

    def _new_repo(self, path):
        return ProjectRepo(path)

    def add_cur_dir(self):
        """Adds the current directory (.) as the first repo in the
        collection. Returns the available project names or raises a GitError
        exception."""
        repo = self._new_repo('.')
        results = repo.find_projects(self.labname)
        self.repos.insert(0, repo)
        return results

    def add_repo(self, path, check_projectname):
        repo = self._new_repo(path)
        repo.find_remote(self.labname, check_projectname)
        self.repos.append(repo)

    def find_repo(self, projectname):
        for repo in self.repos:
            try:
                pkg = repo.find_remote(self.labname, projectname)
                return repo, pkg
            except GitNoRemote:
                pass
        return None, None

    def get_commit(self, commit_id):
        for repo in self.repos:
            try:
                return repo.repo[commit_id]
            except KeyError:
                continue
        return None

    def get_diff(self, commit):
        if not isinstance(commit, str):
            commit = str(commit.id)
        for repo in self.repos:
            if not repo.repo.get(commit):
                continue
            diff = repo.get_diff(commit, silent=True)
            if diff:
                return diff
        return ''
