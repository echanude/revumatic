#!/usr/bin/env python

from setuptools import setup, find_packages

from modules import version

setup(
    name='revumatic',
    version=version.get_version(),
    description='The TUI tool for CentOS/RHEL kernel review.',
    author='Jiri Benc',
    author_email='jbenc@redhat.com',
    url='https://gitlab.com/redhat/centos-stream/src/kernel/utils/revumatic',
    license='GNU General Public License, version 2',
    packages=find_packages(),
    install_requires=[
        'pygit2',
        'python-Levenshtein',
        'pyyaml',
        'urllib3',
    ],
    scripts=['revumatic'],
)
